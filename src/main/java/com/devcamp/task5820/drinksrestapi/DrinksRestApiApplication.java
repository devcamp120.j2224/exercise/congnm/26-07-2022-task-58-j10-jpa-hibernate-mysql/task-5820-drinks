package com.devcamp.task5820.drinksrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;



@SpringBootApplication
public class DrinksRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DrinksRestApiApplication.class, args);
	}

}
