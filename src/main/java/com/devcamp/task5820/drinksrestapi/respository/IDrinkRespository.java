package com.devcamp.task5820.drinksrestapi.respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task5820.drinksrestapi.model.CDrinks;

public interface IDrinkRespository extends JpaRepository <CDrinks, Long> {
    
}
