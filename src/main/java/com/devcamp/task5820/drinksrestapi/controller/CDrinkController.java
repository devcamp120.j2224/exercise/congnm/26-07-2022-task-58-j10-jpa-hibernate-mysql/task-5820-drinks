package com.devcamp.task5820.drinksrestapi.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task5820.drinksrestapi.model.CDrinks;
import com.devcamp.task5820.drinksrestapi.respository.IDrinkRespository;

@RestController
@CrossOrigin
@RequestMapping("/")

public class CDrinkController {
    @Autowired
    IDrinkRespository drinkRepository;

    @GetMapping("/drinks")
    public ResponseEntity<List<CDrinks>> getDrinks(){
        try {
            List<CDrinks> listDrinks = new ArrayList<CDrinks>();
            drinkRepository.findAll().forEach(listDrinks::add);
            if (listDrinks.size() == 0) {
                return new ResponseEntity<List<CDrinks>>(listDrinks, HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<List<CDrinks>>(listDrinks, HttpStatus.OK);
            }
        } catch (Exception ex) {
          
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
